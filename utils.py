# -*- coding: utf-8 -*-
import os
import shutil
from os.path import join, dirname, basename, abspath
from sklearn.datasets import load_files as lf
from load_files import split_str
from nlpir.PyNLPIR import *

nlpir_init()
here = lambda f: join(dirname(abspath(__file__)), f)
stopwords = open(here('stopwords.txt')).read().split()

def nlpir_cut(ustr):
    for s in nlpir_paragraph_process(ustr.encode('utf8')).split():
        try: 
            if s not in stopwords:
                yield s.decode('utf8')
        except: pass

def find_invalid_data(root):
    u'''
    找出所有不良数据文件：空文件，不含'-------baixing\n'的文件等
    '''
    for r, _, files in os.walk(root):
        for f in files:
            content = open(join(r, f)).read().lstrip().rstrip()
            if len(content) == 0:
                yield join(r,f)


def move_invalid_data(root, des):
    u'''
    将所有不符要求的文件移动到des
    '''
    if not os.path.exists(des):
        os.mkdir(des)

    for f in find_invalid_data(root):
        destdir = join(des, basename(dirname(f)))
        if not os.path.exists(destdir):
            os.mkdir(destdir)
        shutil.move(f, destdir)

def convert_to_maxent(datapath, outputf):
    u'''
    将训练数据转换为maxent可用的格式
    '''
    import codecs
    dataset = lf(datapath)
    size = len(dataset.target)
    output = codecs.open(outputf, 'w', 'utf8')
    for i in range(size):
        output.write("%s %s\n" % (dataset.target_names[dataset.target[i]], ' '.join(list(nlpir_cut(dataset.data[i].decode('utf8'))))))
    output.close()

def random_move(orig, dest, limit):
    u'''
    从orig随机移动limit个文件到dest
    '''
    import random, shutil
    files = []
    for d, _, fs in os.walk(orig):
        for f in fs:
            files.append((os.path.basename(d), f))
    random.shuffle(files)
    for d, f in files[:limit]:
        if not os.path.exists(join(dest, d)):
            os.mkdir(join(dest, d))
        shutil.move(join(orig, d, f), join(dest, d, f))
    return limit

if __name__ == '__main__':
    import sys
    orig = sys.argv[1]
    des = join(orig, '-invalid')
    move_invalid_data(orig, des)
