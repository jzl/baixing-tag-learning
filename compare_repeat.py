# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net
统计train data和test data的重复程度
"""

from os import listdir
from os.path import isdir, join

def get_ids(directory_name):
    ids = []
    for f in listdir(directory_name):
        if isdir(join(directory_name, f)):
            ids.extend(listdir(join(directory_name, f)))

    return ids

train_ids = get_ids('/home/liqiping/Documents/mldata-05-20')
test_ids = get_ids('/home/liqiping/Documents/mltestdata-05-20')

common_count = 0
for id in test_ids:
    if id in train_ids:
        common_count += 1

print len(train_ids), len(test_ids), common_count, float(common_count) / len(test_ids)
