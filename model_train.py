# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net

训练分类器
"""

import datetime
import numpy as np
import os

# 选择Naive Bayes或者 SGDClassifier作为分类器
#from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier, LogisticRegression, SGDRegressor
from sklearn.multiclass import OneVsRestClassifier, OutputCodeClassifier
from sklearn.svm import LinearSVC, SVC

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import TruncatedSVD
from sklearn.externals import joblib
from sklearn.datasets import load_files as lf
from load_files import load_title_desc

import jieba
import json
import utils


# 如果有必要，利用各个类目的先验概率进行分类
with open("./class_count.json") as f:
    class_priors = json.load(f)

feature_extrators = {
    'jieba':[('vect', CountVectorizer(tokenizer=jieba.cut,
                                   stop_words=[],
                                   ngram_range=(1, 2),
                                   min_df = 1)),
            ('tfidf', TfidfTransformer()),
            ],
    'nlpir':[('vect', CountVectorizer(tokenizer=utils.nlpir_cut,
                                   stop_words=[],
                                   ngram_range=(1, 2),
                                   min_df = 1)),
             ('tfidf', TfidfTransformer()),
            ],
    }


classfiers = {
    'ovr_sgd':Pipeline(feature_extrators['jieba']+\
                       [
                        ('clf', OneVsRestClassifier(SGDClassifier(loss='modified_huber'), n_jobs = -1)),
                       ]),
    'ovr_sgd_nlpir':Pipeline(feature_extrators['nlpir']+\
                       [
                        ('clf', OneVsRestClassifier(SGDClassifier(loss='modified_huber'), n_jobs = -1)),
                       ]),
    'svc':Pipeline(feature_extrators['jieba']+\
                    [
                        ('clf', SVC(probability=True)),
                    ]),
    'svc_nlpir':Pipeline(feature_extrators['nlpir']+\
                    [
                        ('clf', SVC(probability=True)),
                    ]),
    'ovr_svc':Pipeline(feature_extrators['jieba']+\
                       [
                        ('clf', OneVsRestClassifier(SVC(probability=True), n_jobs = -1)),
                       ]),
    'ovr_svc_nlpir':Pipeline(feature_extrators['nlpir']+\
                       [
                        ('clf', OneVsRestClassifier(SVC(probability=True), n_jobs = -1)),
                       ]),
    'logistic':Pipeline(feature_extrators['nlpir']+\
                        [
                          ('clf', LogisticRegression()),
                        ]),
    'sgd_logistic':Pipeline(feature_extrators['nlpir']+\
                        [
                          ('rdm', SGDClassifier()),
                          ('clf', LogisticRegression()),
                        ]),
    'sgdr_logistic':Pipeline(feature_extrators['nlpir']+\
                        [
                          ('rdm', SGDRegressor()),
                          ('clf', LogisticRegression()),
                        ]),
    'bayes':Pipeline(feature_extrators['nlpir']+\
                        [
                          ('clf', MultinomialNB()),
                        ]),
    'ovr_bayes':Pipeline(feature_extrators['nlpir']+\
                        [
                          ('clf', OneVsRestClassifier(MultinomialNB())),
                        ]),
            }

def get_class_prior(target_names):
    class_prior = []
    for i in range(len(target_names)):
        class_prior.append(class_priors[target_names[i]])

    return class_prior

def fitter(datapath, clf='ovr_sgd_nlpir', limit = None):
    u'''
    加载datapath下的训练数据, 生成并返回classifier.
    :param limit: 限制的训练数据条数
    '''

    data = load_title_desc(datapath, limit)
    X_title = data['title']
    y_train = data['target']
    train_names = data['target_names']
    print "finish load train data, size:%d\n"%len(X_title)

    title_clf = classfiers[clf]
    title_clf.fit(X_title, y_train)
    print 'train finished'

    return title_clf, train_names


def predict(title_clf, train_names, test_data_path, limit = None):
    u'''
    使用已经生成的classifier clf对路径test_data_path下的数据
    :param limit: 限制的测试样本数
    进行预测,并返回测试统计结果:
    不确定却正确的，内容，概率，总计：unsure_correct
    不确定却错误的，内容，概率，总计： unsure_wrong
    确定却错误的，内容，概率，总计：sure_wrong
    '''
    
    # 加载测试数据
    data = lf(test_data_path)
    X_title = data.data
    y_test = data.target
    target_names = data.target_names

    if limit is not None:
        test_size = min(limit, len(y_test))
    else:
        test_size = len(y_test)
    X_title = X_title[:test_size]
    y_test = y_test[:test_size]
    # 预测
    title_classes = title_clf.predict_proba(X_title)

    # 初始化统计结果
    unsure_correct = []
    unsure_wrong = []
    sure_wrong = []
    sure_correct = []
    correct_count = 0
    score_count = 0

    for i in range(test_size):
        # 根据第i的样本的结果按照概率大小排序, 取最大的前三项
        sorted_labels = np.argsort(title_classes[i])[-1:-4:-1]

        selected_labels = [train_names[j] for j in sorted_labels]
        actual_label = target_names[y_test[i]] #真实的类目

        # 根据最大的预测概率是否大于0.2来决定这次的分类结果是否确定
        max_prob = title_classes[i][sorted_labels]
        is_sure = max_prob[0] >= 0.2

        if actual_label in selected_labels:
            correct_count += 1
            if not is_sure:
                unsure_correct.append((max_prob, X_title[i], actual_label, selected_labels))
            else:
                sure_correct.append((max_prob, X_title[i], actual_label, selected_labels))

        else:
            if is_sure:
                sure_wrong.append((max_prob, X_title[i], actual_label, selected_labels))
            else:
                unsure_wrong.append((max_prob, X_title[i], actual_label, selected_labels))

        if actual_label == selected_labels[0]:
            score_count += 1

    return {'test_size': test_size, 'correct_count': correct_count,
            'score_count': score_count, 'unsure_wrong':unsure_wrong,
            'sure_wrong': sure_wrong, 'unsure_correct': unsure_correct,
            'score': title_clf.score(X_title, y_test), 'sure_correct':sure_correct}


def train(datapath, outputdir, append_time=True):
    u'''
    加载datapath下的训练数据，生成classifier,并将classifier持久化
    :param datapath: 训练数据所在目录
    :param clf_pkl: classifier持久化文件(.pkl)路径
    :param names_pkl: 训练数据持久化文件(.pkl)路径
    :param append_time: 是否在持久化文件名后附加日期时间
    '''
    outputdir = os.path.join(outputdir, datetime.datetime.now().strftime("%m%d%H%M"))
    if not os.path.exists(outputdir):
        os.mkdir(outputdir)
    names_pkl = os.path.join(outputdir, 'names.pkl')
    clf_pkl =   os.path.join(outputdir, 'clf.pkl')
    
    title_clf, train_names = fitter(datapath)
    joblib.dump(title_clf, clf_pkl)
    joblib.dump(train_names, names_pkl)
    return clf_pkl, names_pkl

if __name__ == "__main__":
    train('/home/ls/mldata/mltitledata', './data')
    #from web.hello import run
    #run(*fitter('/home/ls/mldata/mldata-08-23/'))
