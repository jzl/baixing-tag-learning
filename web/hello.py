# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net

一个简单的Flask服务器用于展示自动推荐类目的效果
用于输入一条信息的标题，能够自动推荐三个最有可能的类目
"""

from flask import Flask
from flask import render_template
from flask import request
from flask import url_for

from sklearn.externals import joblib

import json
from os.path import join, abspath, dirname
import numpy as np

title_clf = None
train_names = None
print "finish load model"


first_category_name = {
    "qiuzhi": u"求职简历",
    "jiaoyupeixun": u"教育培训",
    "jianzhi": u"兼职招聘",
    "huodong": u"交友活动",
    "gongzuo": u"全职招聘",
    "fuwu": u"生活服务",
    "fang": u"房屋租赁",
    "ershou": u"物品交易",
    "chongwuleimu": u"宠物",
    "cheliang": u"车辆买卖",
}

here = dirname(abspath(__file__))
with open(join(here,"../namemap.json")) as f:
    namemap = json.load(f)


with open(join(here, "../parent_map.json")) as f:
    parentmap = json.load(f)


app = Flask(__name__)


def get_category_names(title):
    title_classes = title_clf.predict_proba([title])
    sorted_labels = np.argsort(title_classes[0]).tolist()
    sorted_labels.reverse()

    return  [(namemap[train_names[j]], first_category_name[parentmap[train_names[j]]])
             for j in sorted_labels[:3]]


@app.route("/", methods=["POST", "GET"])
def hello():
    if request.method == "GET":
        print url_for("hello")
        names = None
        title = ""
    else:
        title = request.form["title"]
        print type(title), title
        names = get_category_names(title.encode("utf-8"))
    return render_template("hello.html", names=names, title = title)

def run(clf_pkl, names_pkl):
    global title_clf, train_names
    if title_clf is None:
        title_clf = joblib.load(clf_pkl) if isinstance(clf_pkl, str) else clf_pkl
    if train_names is None:
        train_names = joblib.load(names_pkl) if isinstance(names_pkl, str) else names_pkl
    app.run(debug=True, host="127.0.0.1")

if __name__ == "__main__":
    import sys
    sys.path.insert(0, '..')
    run(*sys.argv[1:3])
