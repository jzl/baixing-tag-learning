# -*- coding=utf-8 -*-

import gearman
import time
#import thread
import multiprocessing

from sklearn.datasets import load_files
from config import test_data_path

data = load_files(test_data_path)
title = data.data[:2000]
print "finish load title"

#def check_request_status(job_request):
    #if job_request.complete:
        #print "Job %s finished!  State: %s" % (job_request.job.unique, job_request.state)
        #result = json.loads(job_request.result)
        #for item in result:
            #print "%s > %s: %s" % (item[0], item[1], item[2])
    #elif job_request.timed_out:
        #print "Job %s timed out!" % job_request.unique
    #elif job_request.state == gearman.JOB_UNKNOWN:
        #print "Job %s connection failed!" % job_request.unique

def worker(start, end):
    name = multiprocessing.current_process().name
    print name, 'Starting'
    for tit in title[start:end]:
        gm_client.submit_job("category_learning", tit)
    print name, 'Exiting'

gm_client = gearman.GearmanClient(['192.168.1.40'])

#time_cost = 0
#for j in range(5):
start_time = time.time()
jobs = []
for i in range(20):
    start = i * 100
    end = (i + 1) * 100
    p = multiprocessing.Process(target=worker, args=(start, end))
    jobs.append(p)
for job in jobs:
    job.start()
for job in jobs:
    job.join()

print time.time() - start_time, "seconds"
    #for i in range(3):
        #start = i * 400
        #end = (i + 1) * 400
        #thread.start_new_thread(worker, (start, end))

    #time_cost += time.time() - start_time
    #print i, "finish job"
    #check_request_status(completed_job_request)

#print time_cost / 5, "seconds"
