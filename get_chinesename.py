# -*- coding=utf-8 -*-

"""
author: liqiping@baixing.net
根据http://www.baixing.com/pages/test/category_meta.php上的内容提取二级类目
中文名与英文名的对应关系，保存成一个json文件
"""

import json
from pyquery import PyQuery as pq

d = pq(filename='./parentname.html')
trs = d("tr")

namemap = {}

for i in range(1, len(trs)):
    tr = trs.eq(i)
    tds = tr.find("td")
    if tds is None or len(tds) == 0:
        continue

    english_name = tds.eq(1).text()
    chinese_name = tds.eq(2).text()

    namemap[english_name] = chinese_name

with open("./namemap.json", "w") as f:
    json.dump(namemap, f)
