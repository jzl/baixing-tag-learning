# -*- coding=utf-8 -*-
"""
author: liqiping@baixing.net
把二级类目的数据集合并为一级类目的数据集
"""

from sklearn.externals import joblib
from os import listdir, mkdir
from os.path import isdir, join
from distutils.dir_util import copy_tree


def combine_folds(directory_name):
    parent_directory = directory_name + "_parent"
    mkdir(parent_directory)

    for parentname in parentlist:
        mkdir(join(parent_directory, parentname))

    for f in listdir(directory_name):
        if isdir(join(directory_name, f)) and f in parentmap:
            parentname = parentmap[f]
            src = join(directory_name, f)
            dist = join(parent_directory, parentname)
            copy_tree(src, dist)


parentlist = joblib.load("./parentlist.pkl")
parentmap = joblib.load("./parentmap.pkl")

train_dir = "./mldata"
combine_folds(train_dir)

test_dir = "./mltestdata"
combine_folds(test_dir)
